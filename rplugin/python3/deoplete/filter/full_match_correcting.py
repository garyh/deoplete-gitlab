# ============================================================================
# FILE: full_match_correcting.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import re
from difflib import SequenceMatcher
from deoplete.base.filter import Base
from deoplete.util import load_external_module, fuzzy_escape
from deoplete.util import Nvim, UserContext, Candidates, Candidate

load_external_module(__file__, "sources/gitlab")

from logger import init_logger

logger = init_logger(__name__)

GOOD_ENOUGH = 0.6 # difflib defines >= 0.6 as a close match

def close_match(a, b):
  return SequenceMatcher(lambda x: x == ' ', a, b).ratio() > GOOD_ENOUGH

class Matcher:
  """
  Defines how we match a single Candidate
  """

  def __init__(self, word_p, target, norm):
    self.word_p = word_p
    self.target = target
    self.norm = norm

  def matches(self, candidate: Candidate) -> bool:
    word = self.norm(candidate['word'])

    if self.word_p.search(word):
      return True

    menu = Matcher.case_insensitive(candidate['menu'])

    if self.target in menu:
      return True

    return close_match(self.target, menu)

  @staticmethod
  def case_insensitive(string: str) -> str:
    return string.lower()

  @staticmethod
  def case_sensitive(string: str) -> str:
    return string

class Filter(Base):
  """
  This defines the gitlab_full_match_correcting matcher.

  This matcher is able to suggest values that do not match on word, but include match information
  in the menu
  """
  def __init__(self, vim: Nvim) -> None:
    super().__init__(vim)

    self.name = 'gitlab_full_match_correcting'
    self.description = 'full fuzzy matcher, matching non-word parts'
    self.debug = logger.debug
    logger.debug('Constructed gitlab_full_match_correcting filter')

  def filter(self, context: UserContext) -> Candidates:
    complete_str = context['complete_str']
    unprefixed = complete_str.strip('@~#!"')
    complete_str = unprefixed
    if context['ignorecase']:
      complete_str = complete_str.lower()
    self.debug('Matching against %s', complete_str)

    word_p = re.compile(fuzzy_escape(complete_str, context['camelcase']))
    norm = Matcher.case_insensitive if context['ignorecase'] else Matcher.case_sensitive

    matcher = Matcher(word_p, unprefixed, norm)

    return [x for x in context['candidates'] if matcher.matches(x)]

# vim: sw=2 ts=2
