# ============================================================================
# FILE: logger.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import logging

handler = logging.FileHandler('/tmp/deoplete-gitlab.log', encoding='utf-8')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

def init_logger(name):
  logger = logging.getLogger(name)
  logger.addHandler(handler)
  logger.setLevel(logging.DEBUG)
  return logger

# vim: sw=2 ts=2
