# ============================================================================
# FILE: candidate.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

from dataclasses import dataclass

@dataclass(eq=True,frozen=True)
class Candidate:
  """
  Represent (and parse) candidates
  """
  word: str
  menu: str

  def dict(self):
    return { 'word': self.word, 'menu': self.menu }

  def parse(self, results):
    for x in results:
      yield Candidate(self.word.format(**x), self.menu.format(**x))

# vim: sw=2 ts=2
