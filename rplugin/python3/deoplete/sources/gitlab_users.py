# ============================================================================
# FILE: gitlab-users.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import re
from deoplete.util import load_external_module

load_external_module(__file__, "sources/gitlab")

from workers import GLUsersBackgroundWorker
from gitlab_source import GitlabSource
from logger import init_logger

logger = init_logger(__name__)

USER_PATTERN = re.compile(r'@[\w\d\._-]{3,}')

class Source(GitlabSource):
  """Fetches users from GitLab API."""

  def __init__(self, vim):
    GitlabSource.__init__(self, vim, USER_PATTERN)

    self.name = 'gitlab-users'
    logger.debug('Created new gitlab-users source')

  def gather_candidates(self, context):
    base = self.api_base
    project_path = self.project_path

    if base is None:
      return []

    logger.debug('base=%s', base)
    search_strs = re.findall(USER_PATTERN, context['input'] or '')

    for search_str in search_strs:
      username = search_str.strip('@')[0:3]
      logger.debug('search_str=%s', username)
      key = '{0}:{1}:{2}'.format(base, project_path, username)
      self.start_worker(GLUsersBackgroundWorker, key, (project_path, username))

    return self.all_results()

# vim: sw=2 ts=2
