# ============================================================================
# FILE: gitlab-labels.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import re
from deoplete.util import load_external_module

load_external_module(__file__, "sources/gitlab")

from workers import GLLabelsBackgroundWorker
from gitlab_source import GitlabSource
from logger import init_logger

logger = init_logger(__name__)

LABEL_PATTERN = re.compile(r'~([:\w\d\._-]{3,}|"[:\s\w\d\._-]{3,}"?)')

class Source(GitlabSource):
  """Fetches labels from GitLab API."""

  def __init__(self, vim):
    GitlabSource.__init__(self, vim, LABEL_PATTERN)

    self.name = 'gitlab-labels'
    logger.debug('Created new gitlab-labels source')

  def gather_candidates(self, context):
    if self.api_base is None:
      return []

    inputs = re.findall(LABEL_PATTERN, context['input'] or '')

    for i in inputs:
      search_str = i.strip('~"')
      logger.debug('search_str=%s', search_str)
      key = '{0}:{1}:{2}'.format(self.api_base, self.project_path, search_str)
      self.start_worker(GLLabelsBackgroundWorker, key, (self.project_path, search_str))

    return self.all_results()

# vim: sw=2 ts=2
