# Deoplete for GitLab

Auto-completion support for GitLab markdown references.

[![asciicast](https://asciinema.org/a/3n3ar14HpVcrUQzfjYCoYwiDT.png)](https://asciinema.org/a/3n3ar14HpVcrUQzfjYCoYwiDT?t=5&speed=2)

## Dependencies

This does require the following plugins to be installed

- [deoplete][]: the auto-completion framework
- [fugitive][]: used to determine the current project

## Installation

Install with your favourite vim plugin manager:

```vim
Plug 'https://gitlab.com/alexkalderimis/deoplete-gitlab', { 'do': './bin/install.sh' }
```

## Configuration

The plugin provides the following sources:

- `gitlab_users`
- `gitlab_merge_requests`
- `gitlab_labels`

These are configured to be active on the following filetypes:

- `gitcommit`
- `markdown`
- `pandoc`
- `vimwiki`

But they can be disabled/added as needed. See the `:help deoplete` documentation
for more details.

You may need to provide an API token, in order to read confidential resources.
This is a bit of a work-in-progress.

# Usage

Whenever you are writing markdown-ish text, typing GitLab references (such as
`@user`, `!123`, `~label::name`) will trigger auto-completion.

Unlike the GitLab web UI, the completion will consider suggestions which match
what you type, but provide a different value. An example of this is users, which
allow you to type the user's real name, and choose the suggested username, or
merge-requests, where you can type `!my-branch-name` and the suggestions will
include the MR for that branch (if it is open).

To save bandwidth, and spare the poor servers, results are cached for up to an
hour, so suggestions may be slightly stale.

[fugitive]: https://github.com/tpope/vim-fugitive
[deoplete]: https://github.com/Shougo/deoplete.nvim
